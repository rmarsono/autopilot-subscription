export const dataPlanNames = {
  0: 'Silver',
  1: 'Gold',
  2: 'Platinum',
  3: 'Platinum+'
}

export const dataPlanDescriptions = {
  0: '1,000 contacts monthly',
  1: '5,000 contacts monthly',
  2: '20,000 contacts monthly',
  3: '40,000 contacts monthly'
}

export const dataAddOnNames = {
  0: 'Priority support',
  1: 'Salesforce CRM integration',
  2: 'Custom IP',
  3: 'Activity streams'
}

export const dataAddOnPrices = {
  0: '$199/month',
  1: '$199/month',
  2: '$149 each/month',
  3: '$149/month'
}

export const dataCurrentPlan = 0
