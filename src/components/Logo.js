import React from 'react'
import { Link } from 'react-router-dom'

const Logo = () => {
  return (
    <Link to='/' className='logo'>
      <span className='logo__copy'>Autopilot</span>
    </Link>
  )
}

export default Logo
