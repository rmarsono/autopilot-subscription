import React from 'react'
import { Link } from 'react-router-dom'

const Nav = () => {
  return (
    <nav>
      <Link to='/subscription'>Subscription & billing</Link>
      <Link to='/log-out'>Log out</Link>
    </nav>
  )
}

export default Nav
