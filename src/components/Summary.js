import React, { useContext } from 'react'

import AppContext from './AppContext'

const Summary = () => {
  const { planNames, addOnNames, selectedPlan, selectedAddOns } = useContext(
    AppContext
  )

  return (
    <section id='summary' className='aside__inner'>
      <h2>Summary</h2>
      <h3>Plan Selected</h3>
      {planNames[selectedPlan]}
      <h3>Add-ons Selected</h3>
      <ul className='selected-add-ons'>
        {selectedAddOns.map((addOnKey, index) => {
          return <li key={index}>{addOnNames[addOnKey]}</li>
        })}
      </ul>
    </section>
  )
}

export default Summary
