import React, { useContext } from 'react'

import AppContext from './AppContext'

const PlanSelector = () => {
  const {
    planNames,
    planDescriptions,
    selectedPlan,
    setSelectedPlan
  } = useContext(AppContext)

  const handleChange = event => {
    setSelectedPlan(event.currentTarget.value)
  }

  return (
    <section id='plan-selector' className='inner-content-wrap'>
      <h2>Plan</h2>
      <select
        onChange={handleChange}
        value={selectedPlan}
        className='select--css'
      >
        {Object.keys(planNames).map(key => {
          return (
            <option key={key} value={key}>
              {planNames[key]} {planDescriptions[key]}
            </option>
          )
        })}
      </select>
      <span className='span--help'>
        Only plans you can downgrade or upgrade to are shown.
      </span>
    </section>
  )
}

export default PlanSelector
