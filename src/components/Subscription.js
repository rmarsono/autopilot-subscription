import React from 'react'

import Summary from './Summary'
import PlanSelector from './PlanSelector'
import AddOnSelector from './AddOnSelector'

const Subscription = () => {
  return (
    <React.Fragment>
      <h1>Change subscription</h1>
      <div className='main-wrap'>
        <section className='content-wrap'>
          <PlanSelector />
          <AddOnSelector />
        </section>
        <aside>
          <Summary />
        </aside>
      </div>
    </React.Fragment>
  )
}

export default Subscription
