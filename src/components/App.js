import React, { useState, useEffect } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import AppContext from './AppContext'
import Header from './Header'
import Subscription from './Subscription'
import LogOut from './LogOut'
import Breadcrumbs from './Breadcrumbs'

import {
  dataPlanNames,
  dataPlanDescriptions,
  dataAddOnNames,
  dataAddOnPrices,
  dataCurrentPlan
} from '../data'

const App = () => {
  const [planNames, setPlanNames] = useState({})
  const [planDescriptions, setPlanDescriptions] = useState({})
  const [addOnNames, setAddOnNames] = useState({})
  const [addOnPrices, setAddOnPrices] = useState({})
  const [selectedPlan, setSelectedPlan] = useState(0)
  const [selectedAddOns, setSelectedAddOns] = useState([])

  useEffect(() => {
    if (dataPlanNames.hasOwnProperty(dataCurrentPlan))
      delete dataPlanNames[dataCurrentPlan]
    setPlanNames(dataPlanNames)
    setPlanDescriptions(dataPlanDescriptions)
    setAddOnNames(dataAddOnNames)
    setAddOnPrices(dataAddOnPrices)
    setSelectedPlan(Object.keys(dataPlanNames)[0])
  }, [])

  return (
    <AppContext.Provider
      value={{
        planNames,
        planDescriptions,
        addOnNames,
        addOnPrices,
        selectedPlan,
        selectedAddOns,
        setSelectedPlan,
        setSelectedAddOns
      }}
    >
      <BrowserRouter>
        <Header />
        <Breadcrumbs />
        <Switch>
          <Route exact path='/' component={Subscription} />
          <Route exact path='/subscription' component={Subscription} />
          <Route exact path='/log-out' component={LogOut} />
        </Switch>
      </BrowserRouter>
    </AppContext.Provider>
  )
}

export default App
