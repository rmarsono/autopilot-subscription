import React, { useContext } from 'react'

import AppContext from './AppContext'

const AddOnSelector = () => {
  const {
    addOnNames,
    addOnPrices,
    selectedAddOns,
    setSelectedAddOns
  } = useContext(AppContext)

  const handleChange = event => {
    const { value } = event.currentTarget
    if (selectedAddOns.indexOf(value) >= 0) {
      const newSelectedAddOns = [...selectedAddOns]
      newSelectedAddOns.splice(selectedAddOns.indexOf(value), 1)
      setSelectedAddOns(newSelectedAddOns)
    } else setSelectedAddOns([...selectedAddOns, value])
  }

  return (
    <section id='add-ons-selector' className='inner-content-wrap'>
      <h2>Add-ons</h2>
      {Object.keys(addOnNames).map(key => {
        return (
          <div key={key} className='checkbox-wrap'>
            <input
              id={`add-on-${key}`}
              className='checkbox-wrap__checkbox'
              type='checkbox'
              name={addOnNames[key]}
              onChange={handleChange}
              value={key}
              checked={selectedAddOns.indexOf(key) >= 0}
            />
            <label htmlFor={`add-on-${key}`}>
              {addOnNames[key]} - {addOnPrices[key]}
            </label>
          </div>
        )
      })}
    </section>
  )
}

export default AddOnSelector
