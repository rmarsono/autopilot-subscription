import React from 'react'

const Breadcrumbs = () => {
  return (
    <section id='breadcrumbs'>
      <button onClick={() => window.history.back()}>
        <span>Go back</span>
      </button>
    </section>
  )
}

export default Breadcrumbs
