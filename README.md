# Autopilot Subscription (test) by Rendy Marsono

Thank you for the opportunity to do this test!

## Notes

1. The dummy data is loaded in `componentDidMount` (using the `useEffect` hook) from **src/data/data.js** to simulate loading from a back-end API.
1. I used **reset.css** to remove built-in browser styling.
1. I don't have the **Karbon Semibold font** installed so I have chosen a Google font instead (Catamaran).

## Assumptions

1. I have made some assumptions on the plans and what they include.
1. I have made some assumptions on the data structure of the plans and add-ons. The dummy data is stored in **src/data/data.js**.
1. When the app loads, it simulates grabbing the **current plan** and removes this from the list of all available plans as per the requirement. At the moment, the **current plan** is set to **'Silver'**.
1. The **subscription** link in the header takes you to **/subscription** which is where the main app lives. The root path **/** also loads this main app. The **Log out** link loads the **LogOut** component which doesn't have any content apart from the heading.
1. I have used these breakpoints: tablet (768px) and desktop (1200px)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

---

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. I am using port 8811 to avoid conflict with other stuff I am building at the moment.<br>
Open [http://localhost:8811](http://localhost:8811) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
